import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Providerregistration1Page } from './providerregistration1.page';

const routes: Routes = [
  {
    path: '',
    component: Providerregistration1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Providerregistration1PageRoutingModule {}
