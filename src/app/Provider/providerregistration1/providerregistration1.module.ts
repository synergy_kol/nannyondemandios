import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Providerregistration1PageRoutingModule } from './providerregistration1-routing.module';

import { Providerregistration1Page } from './providerregistration1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Providerregistration1PageRoutingModule
  ],
  declarations: [Providerregistration1Page]
})
export class Providerregistration1PageModule {}
