import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProviderbookingsPageRoutingModule } from './providerbookings-routing.module';

import { ProviderbookingsPage } from './providerbookings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProviderbookingsPageRoutingModule
  ],
  declarations: [ProviderbookingsPage]
})
export class ProviderbookingsPageModule {}
