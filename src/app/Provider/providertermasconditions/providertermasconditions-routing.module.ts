import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProvidertermasconditionsPage } from './providertermasconditions.page';

const routes: Routes = [
  {
    path: '',
    component: ProvidertermasconditionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProvidertermasconditionsPageRoutingModule {}
