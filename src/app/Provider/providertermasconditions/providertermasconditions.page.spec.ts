import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProvidertermasconditionsPage } from './providertermasconditions.page';

describe('ProvidertermasconditionsPage', () => {
  let component: ProvidertermasconditionsPage;
  let fixture: ComponentFixture<ProvidertermasconditionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidertermasconditionsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProvidertermasconditionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
