import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Providerregistration3PageRoutingModule } from './providerregistration3-routing.module';

import { Providerregistration3Page } from './providerregistration3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Providerregistration3PageRoutingModule
  ],
  declarations: [Providerregistration3Page]
})
export class Providerregistration3PageModule {}
