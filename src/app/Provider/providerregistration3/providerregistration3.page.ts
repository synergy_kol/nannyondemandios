import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-providerregistration3',
  templateUrl: './providerregistration3.page.html',
  styleUrls: ['./providerregistration3.page.scss'],
})
export class Providerregistration3Page implements OnInit {

  pop = false;

  experience: any = '';
  ratetype: any = '';
  rate: any = '';
  availableList: any = [];
  selectDay: any = '';
  selectTime: any = '';
  interest_in: any = '';

  data: any = [];
  userData: any = [];
  preData: any = '';
  isDisabled: boolean = false;
  isLoading = false;

  profileData: any = [];
  hideextra = true;

  userId: any = '';

  age: any;
  userdata: any = [];

  step3Data: any = [];

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private storage: Storage,
    private authService: AuthServiceProvider
  ) { }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);
        } else {
          this.showLoader('Logging out...');
          setTimeout(() => {
            this.storage.remove('userDetails');
            this.hideLoader();
            this.navCtrl.navigateRoot('/welcome');
          }, 3000);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  getDetails() {
    this.storage.get('providerRegsData').then((result) => {
      this.hideLoader();
      this.preData = result;
      console.log('PreStep Data:', this.preData);
    })
  }

  ionViewWillEnter() {
    this.showLoader('Loading...');
    this.storage.get('userDetails').then((val) => {
      if (val != null) {
        this.userId = val.user_id;
        this.hideextra = false;
        this.userData = val;
        this.getProfileDetails();
      } else {
        this.storage.get('setp3').then((val) => {
          this.step3Data = val;
          this.interest_in = this.step3Data.interest_in;
          this.experience = this.step3Data.experience;
          this.ratetype = this.step3Data.rate_type;
          this.rate = this.step3Data.rate;
        });
      }
    });
    this.getDetails();
    this.loginToken();
  }

  getProfileDetails() {
    var body = {
      user_id: this.userData.user_id,
    };
    this.authService.postData("get-profile", body).then(result => {
      this.data = result;
      console.log("profile: ", this.data);
      this.profileData = this.data.result.data;
      this.interest_in = this.profileData.interest_in;
      this.experience = this.profileData.experience;
      this.ratetype = this.profileData.rate_type;
      this.rate = this.profileData.rate;
      this.availableList = this.profileData.availabilities;
      this.hideLoader();
    },
      error => {
        this.hideLoader();
      });
  }

  openPop() {
    this.pop = true;
  }


  closepop() {
    this.pop = false;
  }

  onSave(findday) {
    var somethingIsNotString = false;
    this.availableList.forEach(function (item) {
      if (item.day == findday) {
        somethingIsNotString = true;
      }
    })
    if (this.selectDay == "") {
      this.presentToast("Please Select Day")
    } else if (this.selectTime == "") {
      this.presentToast("Please Select Time")
    } else {
      this.showLoader('Please wait...');
      if (somethingIsNotString) {
        this.presentToast('Sorry! "' + findday + '" is Already in Availibility List.');
        this.hideLoader();
      } else {
        let data = {
          day: this.selectDay,
          time: this.selectTime
        }
        this.availableList.push(data);
        this.presentToast('Successfully Saved');
        setTimeout(() => {
          this.pop = false;
          this.selectDay = '';
          this.selectTime = '';
          this.hideLoader();
          console.log(this.availableList);
        }, 3000)
      }
    }
  }

  rmSchedule(ind) {
    this.availableList.splice(ind, 1);
    console.log(this.availableList);
  }

  onBack() {
    this.navCtrl.pop();
  }
  onSubmit() {
    if (this.interest_in == '') {
      this.presentToast('Please Choose Interest In.');
    } else if (this.preData.servicetype != 3 && this.experience.trim() == "") {
      this.presentToast('Please enter your Year of Experience.');
    } else if (this.experience >= this.calculateAge(this.preData.dob)) {
      this.presentToast('Please make sure your Year of Experience not greater than or equal with you Age.');
    } else if (this.ratetype == '') {
      this.presentToast('Please Choose Rate Type.');
    } else if (this.rate.trim() == '') {
      this.presentToast('Please enter Rate');
    } else if (this.availableList == '') {
      this.presentToast('Please add minimum One Schedule.');
    } else {
      this.showLoader('Please wait...');
      var body = {
        service_type: this.preData.servicetype,
        f_name: this.preData.fname,
        l_name: this.preData.lname,
        dob: this.preData.dob,
        phone: this.preData.phone,
        emg_phone: this.preData.emgnphone,
        email: this.preData.email,
        password: this.preData.password,
        address: this.preData.address,
        state: this.preData.state,
        city: this.preData.city,
        zip: this.preData.zip,
        about_note: this.preData.aboutnote,
        identy_type: this.preData.identytype,
        will_driver: this.preData.willdriver,
        is_smoke: this.preData.issmoke,
        comfortable_pet: this.preData.comfortablepet,
        identity_documents: this.preData.identitydocuments,
        certificates: this.preData.certificates,
        intervention_reports: this.preData.interventionreports,
        home_images: this.preData.homeimages,
        police_doc: this.preData.policeimages,
        website: this.preData.website,
        interest_in: this.interest_in,
        experience: this.experience,
        rate_type: this.ratetype,
        rate: this.rate,
        availability: this.availableList,
      }
      this.authService.postData("provider-registration", body).then(result => {
        this.hideLoader();
        this.data = result;
        this.userData = this.data.result.data;
        console.log("Registration: ", this.data);
        if (this.data.status.error_code == 0) {
          this.storage.remove('providerRegsData');
          this.storage.remove('setp1');
          this.storage.remove('setp2');
          this.storage.remove('setp3');
          this.presentToast("Thanks! You has been succesfully Registered.");
          this.storage.set("userDetails", this.userData);
          this.navCtrl.navigateRoot('/providercongratulation');
        } else {
          this.presentToast(this.data.status.message);
          var object = {
            interest_in: this.interest_in,
            experience: this.experience,
            rate_type: this.ratetype,
            rate: this.rate,
          }
          this.storage.set("setp3", object);
        }
      },
        error => {
          this.hideLoader();
        });
    }
  }

  calculateAge(dob) {
    this.age = dob;
    var dob_entry = this.preData.dob;
    var split_dob = dob_entry.split("T");
    var date_only = split_dob[0];
    var split_date_only = date_only.split("-");
    var month = split_date_only[1];
    var day = split_date_only[2];
    var year = split_date_only[0];
    var today = new Date();
    var age = today.getFullYear() - year;
    if (today.getMonth() < month || (today.getMonth() == month && today.getDate() < day)) {
      age--;
    }
    return age;
  }

  oneditSave() {
    if (this.experience.trim() == "") {
      this.presentToast('Please enter your Year of Experience.');
    } else if (this.experience >= this.calculateAge(this.preData.dob)) {
      this.presentToast('Please make sure your Year of Experience not greater than or equal with you Age.');
    } else if (this.ratetype == '') {
      this.presentToast('Please Choose Rate Type.');
    } else if (this.rate.trim() == '') {
      this.presentToast('Please enter Rate');
    } else if (this.availableList == '') {
      this.presentToast('Please add minimum One Schedule.');
    } else {
      this.showLoader('Please wait...');
      var body = {
        user_id: this.userId,
        service_type: this.preData.servicetype,
        f_name: this.preData.fname,
        l_name: this.preData.lname,
        dob: this.preData.dob,
        phone: this.preData.phone,
        emg_phone: this.preData.emgnphone,
        email: this.preData.email,
        address: this.preData.address,
        state: this.preData.state,
        city: this.preData.city,
        zip: this.preData.zip,
        about_note: this.preData.aboutnote,
        identy_type: this.preData.identytype,
        will_driver: this.preData.willdriver,
        is_smoke: this.preData.issmoke,
        comfortable_pet: this.preData.comfortablepet,
        identity_documents: this.preData.identitydocuments,
        certificates: this.preData.certificates,
        intervention_reports: this.preData.interventionreports,
        home_images: this.preData.homeimages,
        police_doc: this.preData.policeimages,
        interest_in: this.interest_in,
        experience: this.experience,
        rate_type: this.ratetype,
        rate: this.rate,
        availability: this.availableList,
      }
      this.authService.postData("update-provider", body).then(result => {
        this.hideLoader();
        this.data = result;
        this.userData = this.data.result.data;
        console.log("Edit Registration: ", this.data);
        if (this.data.status.error_code == 0) {
          this.storage.remove('providerRegsData');
          this.presentToast("Thanks! You has been succesfully Updated.");
          this.navCtrl.navigateRoot('/dashboard');
        } else {
          this.presentToast(this.data.status.message);
        }
      },
        error => {
          this.hideLoader();
        });
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
