import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Providerregistration2PageRoutingModule } from './providerregistration2-routing.module';

import { Providerregistration2Page } from './providerregistration2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Providerregistration2PageRoutingModule
  ],
  declarations: [Providerregistration2Page]
})
export class Providerregistration2PageModule {}
