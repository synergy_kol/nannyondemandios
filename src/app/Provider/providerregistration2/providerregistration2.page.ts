import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, LoadingController, NavController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Camera, } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

declare var cordova: any;

@Component({
  selector: 'app-providerregistration2',
  templateUrl: './providerregistration2.page.html',
  styleUrls: ['./providerregistration2.page.scss'],
})
export class Providerregistration2Page implements OnInit {

  about: any = '';
  identitytype: any = '';
  checkdrive = false;
  ischeckdrive: any = 'No';
  checksmoke = false;
  ischecksmoke: any = 'No';
  checkpets = false;
  ischeckpets: any = 'No';
  checkpolice = false;

  preData: any = [];
  isDisabled: boolean = false;
  isLoading = false;

  serveridentityImages: any = [];
  identityImages: any = [];
  servercertificateImages: any = [];
  certificateImages: any = [];
  serverinterventionImages: any = [];
  interventionImages: any = [];
  serverhomeImages: any = [];
  homeImages: any = [];
  serverpoliceImages: any = [];
  policeImages: any = [];
  website: any = [];

  userdata: any = [];
  data: any = [];
  profileData: any = [];
  hideextra = true;

  step2Data: any = [];

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private storage: Storage,
    private camera: Camera,
    private actionSheet: ActionSheetController,
    private platform: Platform,
    private filePath: FilePath,
    private file: File,
    private authService: AuthServiceProvider,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);
        } else {
          this.showLoader('Logging out...');
          setTimeout(() => {
            this.storage.remove('userDetails');
            this.hideLoader();
            this.navCtrl.navigateRoot('/welcome');
          }, 3000);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }


  getDetails() {
    this.storage.get('providerRegsData').then((result) => {
      this.hideLoader();
      this.preData = result;
      console.log('PreStep Data:', this.preData);
    })
  }

  ionViewWillEnter() {
    this.showLoader('Loading...');
    this.storage.get('userDetails').then((val) => {
      if (val != null) {
        this.hideextra = false;
        this.userdata = val;
        this.getProfileDetails();
      } else {
        this.storage.get('setp2').then((val) => {
          this.step2Data = val;
          this.about = this.step2Data.aboutnote;
          this.identitytype = this.step2Data.identytype;
          if (this.step2Data.willdriver == "Yes") {
            this.checkdrive = true;
          } else {
            this.checkdrive = false;
          }
          if (this.step2Data.issmoke == "Yes") {
            this.checksmoke = true;
          } else {
            this.checksmoke = false;
          }
          if (this.step2Data.comfortablepet == "Yes") {
            this.checkpets = true;
          } else {
            this.checkpets = false;
          }
        });
        this.website = this.step2Data.website;
      }
    });
    this.getDetails();
    this.loginToken();
  }

  getProfileDetails() {
    var body = {
      user_id: this.userdata.user_id,
    };
    this.authService.postData("get-profile", body).then(result => {
      this.data = result;
      console.log("profile: ", this.data);
      this.profileData = this.data.result.data;
      this.about = this.profileData.about_note;
      this.identitytype = this.profileData.identy_type;
      if (this.profileData.will_driver == "Yes") {
        this.checkdrive = true;
      } else {
        this.checkdrive = false;
      }
      if (this.profileData.is_smoke == "Yes") {
        this.checksmoke = true;
      } else {
        this.checksmoke = false;
      }
      if (this.profileData.comfortable_pet == "Yes") {
        this.checkpets = true;
      } else {
        this.checkpets = false;
      }
      if (this.profileData.police_doc != "") {
        this.checkpolice = true;
      } else {
        this.checkpolice = false;
      }
      this.serveridentityImages = this.profileData.documents;
      this.servercertificateImages = this.profileData.certificated;
      this.serverinterventionImages = this.profileData.intervention;
      this.serverhomeImages = this.profileData.home;
      this.serverpoliceImages = this.profileData.police_doc;
      this.website = this.profileData.website;
      this.hideLoader();
    },
      error => {
        this.hideLoader();
      });
  }

  rmserIdentityImage(id, type) {
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Remove? It will be removed permanently.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Wait...');
            setTimeout(() => {
              var body = {
                image_id: id,
                image_type: type
              };
              this.authService.postData("remove-image", body).then(result => {
                this.data = result;
                console.log("Removed: ", this.data);
                this.hideLoader();
                var body = {
                  user_id: this.userdata.user_id,
                };
                this.authService.postData("get-profile", body).then(result => {
                  this.data = result;
                  this.profileData = this.data.result.data;
                  this.serveridentityImages = this.profileData.documents;
                },
                  error => {
                    this.hideLoader();
                  });
              });
            }, 3000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }
  rmserCertificateImage(id, type) {
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Remove? It will be removed permanently.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Wait...');
            setTimeout(() => {
              var body = {
                image_id: id,
                image_type: type
              };
              this.authService.postData("remove-image", body).then(result => {
                this.data = result;
                console.log("Removed: ", this.data);
                this.hideLoader();
                var body = {
                  user_id: this.userdata.user_id,
                };
                this.authService.postData("get-profile", body).then(result => {
                  this.data = result;
                  this.profileData = this.data.result.data;
                  this.servercertificateImages = this.profileData.certificated;
                },
                  error => {
                    this.hideLoader();
                  });
              });
            }, 3000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }
  rmserInterventionImage(id, type) {
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Remove? It will be removed permanently.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Wait...');
            setTimeout(() => {
              var body = {
                image_id: id,
                image_type: type
              };
              this.authService.postData("remove-image", body).then(result => {
                this.data = result;
                console.log("Removed: ", this.data);
                this.hideLoader();
                var body = {
                  user_id: this.userdata.user_id,
                };
                this.authService.postData("get-profile", body).then(result => {
                  this.data = result;
                  this.profileData = this.data.result.data;
                  this.serverinterventionImages = this.profileData.intervention;
                },
                  error => {
                    this.hideLoader();
                  });
              });
            }, 3000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }
  rmserHomeImage(id, type) {
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Remove? It will be removed permanently.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Wait...');
            setTimeout(() => {
              var body = {
                image_id: id,
                image_type: type
              };
              this.authService.postData("remove-image", body).then(result => {
                this.data = result;
                console.log("Removed: ", this.data);
                this.hideLoader();
                var body = {
                  user_id: this.userdata.user_id,
                };
                this.authService.postData("get-profile", body).then(result => {
                  this.data = result;
                  this.profileData = this.data.result.data;
                  this.serverhomeImages = this.profileData.home;
                },
                  error => {
                    this.hideLoader();
                  });
              });
            }, 3000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }
  rmserPoliceImage(id, type) {
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Remove? It will be removed permanently.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Wait...');
            setTimeout(() => {
              var body = {
                image_id: id,
                image_type: type
              };
              this.authService.postData("remove-image", body).then(result => {
                this.data = result;
                console.log("Removed: ", this.data);
                this.hideLoader();
                var body = {
                  user_id: this.userdata.user_id,
                };
                this.authService.postData("get-profile", body).then(result => {
                  this.data = result;
                  this.profileData = this.data.result.data;
                  this.serverpoliceImages = this.profileData.police_doc;
                },
                  error => {
                    this.hideLoader();
                  });
              });
            }, 3000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  uploadIdentity() {
    let actionSheet = this.actionSheet.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takeIdentity(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          this.takeIdentity(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }
  takeIdentity(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,//for ios only
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      /* let data = {
        image: 'data:image/jpeg;base64,' + imagePath
      } */
      this.identityImages.push('data:image/jpeg;base64,' + imagePath);
    }, (err) => {
      console.log(err);
    });
  }
  rmimage(ind) {
    this.identityImages.splice(ind, 1);
  }

  uploadCertificate() {
    let actionSheet = this.actionSheet.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takeCertificate(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          this.takeCertificate(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }
  takeCertificate(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,//for ios only
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      /* let data = {
        image: 'data:image/jpeg;base64,' + imagePath
      } */
      this.certificateImages.push('data:image/jpeg;base64,' + imagePath);
    }, (err) => {
      console.log(err);
    });
  }
  rmcerimage(ind) {
    this.certificateImages.splice(ind, 1);
  }

  uploadIntervention() {
    let actionSheet = this.actionSheet.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takeIntervention(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          this.takeIntervention(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }
  takeIntervention(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,//for ios only
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      /* let data = {
        image: 'data:image/jpeg;base64,' + imagePath
      } */
      this.interventionImages.push('data:image/jpeg;base64,' + imagePath);
    }, (err) => {
      console.log(err);
    });
  }
  rmintrimage(ind) {
    this.interventionImages.splice(ind, 1);
  }

  uploadHome() {
    let actionSheet = this.actionSheet.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takeHome(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          this.takeHome(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }
  takeHome(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,//for ios only
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      /* let data = {
        image: 'data:image/jpeg;base64,' + imagePath
      } */
      this.homeImages.push('data:image/jpeg;base64,' + imagePath);
    }, (err) => {
      console.log(err);
    });
  }
  rmhomeimage(ind) {
    this.homeImages.splice(ind, 1);
  }

  uploadPolice() {
    let actionSheet = this.actionSheet.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takePolice(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          this.takePolice(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }
  takePolice(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,//for ios only
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      /* let data = {
        image: 'data:image/jpeg;base64,' + imagePath
      } */
      this.policeImages.push('data:image/jpeg;base64,' + imagePath);
    }, (err) => {
      console.log(err);
    });
  }
  rmpoliceimage(ind) {
    this.policeImages.splice(ind, 1);
  }

  driveChange(evt) {
    this.checkdrive == !this.checkdrive;
    if (evt.detail.checked == true) {
      this.ischeckdrive = "Yes";
    } else {
      this.ischeckdrive = "No";
    }
  }
  smokeChange(evt) {
    this.checksmoke == !this.checksmoke;
    if (evt.detail.checked == true) {
      this.ischecksmoke = "Yes";
    } else {
      this.ischecksmoke = "No";
    }
  }
  petChange(evt) {
    this.checkpets == !this.checkpets;
    if (evt.detail.checked == true) {
      this.ischeckpets = "Yes";
    } else {
      this.ischeckpets = "No";
    }
  }

  onBack() {
    this.navCtrl.pop();
  }
  onNext() {
    if (this.about.trim() == '') {
      this.presentToast('Please enter somthing about you');
    } else if (this.identitytype == '') {
      this.presentToast('Please select Indentity Type');
    } else if (this.serveridentityImages.length == 0 && this.identityImages.length == 0) {
      this.presentToast('Please upload Identity Images');
    } else {
      if (this.checkpolice == true) {
        if (this.policeImages.length == 0 && this.serverpoliceImages.length == 0) {
          this.presentToast('Please upload Images of Police Records.');
        } else {
          this.onSteptwo();
        }
      } else {
        this.onSteptwo();
      }
    }
  }

  onSteptwo() {
    this.showLoader('Please wait...');
    var object = {
      servicetype: this.preData.servicetype,
      fname: this.preData.fname,
      lname: this.preData.lname,
      dob: this.preData.dob,
      phone: this.preData.phone,
      emgnphone: this.preData.emgnphone,
      email: this.preData.email,
      password: this.preData.password,
      address: this.preData.address,
      state: this.preData.state,
      city: this.preData.city,
      zip: this.preData.zip,
      aboutnote: this.about,
      identytype: this.identitytype,
      willdriver: this.ischeckdrive,
      issmoke: this.ischecksmoke,
      comfortablepet: this.ischeckpets,
      identitydocuments: this.identityImages,
      certificates: this.certificateImages,
      interventionreports: this.interventionImages,
      homeimages: this.homeImages,
      policeimages: this.policeImages,
      website: this.website
    }
    this.presentToast('Successfully Proceed..');
    this.storage.set("providerRegsData", object);
    this.storage.set("setp2", object);
    setTimeout(() => {
      this.hideLoader();
      this.navCtrl.navigateForward("/providerregistration3");
    }, 100)
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
