import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Providerregistration2Page } from './providerregistration2.page';

const routes: Routes = [
  {
    path: '',
    component: Providerregistration2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Providerregistration2PageRoutingModule {}
