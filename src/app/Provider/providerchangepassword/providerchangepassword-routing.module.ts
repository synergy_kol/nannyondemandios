import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProviderchangepasswordPage } from './providerchangepassword.page';

const routes: Routes = [
  {
    path: '',
    component: ProviderchangepasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProviderchangepasswordPageRoutingModule {}
