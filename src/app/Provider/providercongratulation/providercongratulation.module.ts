import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProvidercongratulationPageRoutingModule } from './providercongratulation-routing.module';

import { ProvidercongratulationPage } from './providercongratulation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProvidercongratulationPageRoutingModule
  ],
  declarations: [ProvidercongratulationPage]
})
export class ProvidercongratulationPageModule {}
