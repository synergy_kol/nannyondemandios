import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProvidercongratulationPage } from './providercongratulation.page';

describe('ProvidercongratulationPage', () => {
  let component: ProvidercongratulationPage;
  let fixture: ComponentFixture<ProvidercongratulationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidercongratulationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProvidercongratulationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
