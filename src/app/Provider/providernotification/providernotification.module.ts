import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProvidernotificationPageRoutingModule } from './providernotification-routing.module';

import { ProvidernotificationPage } from './providernotification.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProvidernotificationPageRoutingModule
  ],
  declarations: [ProvidernotificationPage]
})
export class ProvidernotificationPageModule {}
