import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProviderbookingdetailsPageRoutingModule } from './providerbookingdetails-routing.module';

import { ProviderbookingdetailsPage } from './providerbookingdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProviderbookingdetailsPageRoutingModule
  ],
  declarations: [ProviderbookingdetailsPage]
})
export class ProviderbookingdetailsPageModule {}
