import { Component } from '@angular/core';

import { Platform, MenuController, NavController, LoadingController, AlertController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Router } from '@angular/router';
import { NetworkServiceProvider } from 'src/auth-providers/network-service/network-service';
import { debounceTime } from 'rxjs/operators';
import { MyEvents } from './MyEvents';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  isLoading = false;
  counter = 0;
  data: any = [];
  isConnected: any;

  fname: any = '';
  lname: any = '';
  email: any = '';
  phone: any = '';
  profileimage: any = '';

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menuCtrl: MenuController,
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private alertController: AlertController,
    public authService: AuthServiceProvider,
    private storage: Storage,
    private screenOrientation: ScreenOrientation,
    private router: Router,
    private networkservice: NetworkServiceProvider,
    private toastController: ToastController,
    private events: MyEvents
  ) {
    this.events.getObservable().subscribe((val) => {
      this.fname = val.profile.fname;
      this.lname = val.profile.lname;
      this.email = val.profile.email;
      this.phone = val.profile.phone;
      this.profileimage = val.profile.profile_image;
    });
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.menuCtrl.swipeGesture(false);
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#ffffff');
      this.splashScreen.hide();

      this.networkSubscriber();

      this.storage.get('userDetails').then((val) => {
        console.log('User Details', val);
        if (val != null) {
          this.data = val;
          if (val.role_id == 1) {
            this.navCtrl.navigateRoot('/home');
          } else {
            this.navCtrl.navigateRoot('/dashboard');
          }
        } else {
          this.navCtrl.navigateRoot('/welcome');
        }
      });

      this.platform.backButton.subscribe(() => {
        console.log("URL: ", this.router.url);
        if (this.router.url === '/welcome' || this.router.url === '/home') {
          if (this.counter == 0) {
            this.counter++;
            this.presentToast('Press again to exit');
            setTimeout(() => { this.counter = 0 }, 3000);
          } else {
            navigator['app'].exitApp();
          }
        }
      });

    });
  }

  networkSubscriber(): void {
    this.networkservice.getNetworkStatus().pipe(debounceTime(300))
      .subscribe((connected: boolean) => {
        this.isConnected = connected;
        if (this.isConnected == true) {
          console.log('Connected', this.isConnected);
        } else {
          this.presentToast("SORRY!, You don't have any Network Connection.");
        }
      });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }


  onLogout() {
    const alert = this.alertController.create({
      header: 'Confirm Logout!',
      message: 'Are you sure, you want to Logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 1000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }
}
