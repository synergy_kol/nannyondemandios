import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-forgotpass',
  templateUrl: './forgotpass.page.html',
  styleUrls: ['./forgotpass.page.scss'],
})
export class ForgotpassPage implements OnInit {

  email: any = '';
  isLoading = false;
  data: any;

  constructor(
    public navCtrl: NavController,
    public authService: AuthServiceProvider,
    public loadingController: LoadingController,
    public toastController: ToastController,
  ) { }

  ngOnInit() {
  }

  goLogin() {
    this.navCtrl.pop();
  }
  onForgotpass() {
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.email.trim() == '') {
      this.presentToast('Please enter yout registered Email');
    } else if (!emailPattern.test(this.email)){
      this.presentToast('Wrong Email Format...');
    } else {
      this.showLoader('Please wait...');
      var body = {email: this.email};
      this.authService.postData("forgotPassword", body).then(result => {
        this.data = result;
        console.log("Forgot Pass: ", this.data);
        if (this.data.status.error_code == 0) {
          this.presentToast("Success! Please check your Email. Reset Password link sended to your Email.");
          this.navCtrl.navigateRoot('/login');
        } else {
          this.presentToast(this.data.status.message);
        }
        this.hideLoader();
      },
        error => {
          console.log(error);
          this.hideLoader();
        });
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

}
