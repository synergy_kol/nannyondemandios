import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WrteReviewPageRoutingModule } from './wrte-review-routing.module';

import { WrteReviewPage } from './wrte-review.page';
import { StarRatingModule } from 'ionic5-star-rating';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WrteReviewPageRoutingModule,
    StarRatingModule
  ],
  declarations: [WrteReviewPage]
})
export class WrteReviewPageModule {}
