import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WrteReviewPage } from './wrte-review.page';

const routes: Routes = [
  {
    path: '',
    component: WrteReviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WrteReviewPageRoutingModule {}
