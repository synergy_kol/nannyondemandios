import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-wrte-review',
  templateUrl: './wrte-review.page.html',
  styleUrls: ['./wrte-review.page.scss'],
})
export class WrteReviewPage implements OnInit {

  isDisabled: boolean = false;
  isLoading = false;
  rating: any = 3;
  message: any = '';
  nanny_id: any = '';
  data: any;
  userdata: any = [];

  constructor(
    public navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private activatedRoute: ActivatedRoute,
    private storage: Storage
  ) { }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;        
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);          
        } else {
          this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 3000);
        }
        this.hideLoader();
      },
      error => {
          this.hideLoader();
      });
    });    
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.loginToken();
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((res) => {
      this.nanny_id = res.nanny_id;
      console.log(this.nanny_id);
    });
  }

  logRatingChange(rating) {
    console.log("changed rating: ", rating);
    // do your stuff
  }

  onSubmit() {
    if (this.message.trim() == '') {
      this.presentToast('Please enter Message');
    } else {
      this.showLoader('Please wait...');
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        var body = {
          user_id: this.data.user_id,
          nany_id: this.nanny_id,
          rate: this.rating,
          message: this.message
        };
        this.authService.postData("post-review", body).then(result => {
          this.data = result;
          console.log("post-review: ", this.data);
          this.hideLoader();
          if (this.data.status.error_code == 0) {
            var object = {
              nanny_id: this.nanny_id
            }
            this.navCtrl.navigateBack(["/nannydetails"], { queryParams: object });
            this.presentToast(this.data.status.message);
          } else {
            this.presentToast(this.data.status.message);
          }
        },
          error => {
            this.hideLoader();
          });
      });
      
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
