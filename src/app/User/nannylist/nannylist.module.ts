import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NannylistPageRoutingModule } from './nannylist-routing.module';
import { NannylistPage } from './nannylist.page';
import { StarRatingModule } from 'ionic5-star-rating';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NannylistPageRoutingModule,
    StarRatingModule
  ],
  declarations: [NannylistPage]
})
export class NannylistPageModule {}
