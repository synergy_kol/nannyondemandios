import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuccesspaymentPage } from './successpayment.page';

describe('SuccesspaymentPage', () => {
  let component: SuccesspaymentPage;
  let fixture: ComponentFixture<SuccesspaymentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccesspaymentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuccesspaymentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
