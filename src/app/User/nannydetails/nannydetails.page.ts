import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-nannydetails',
  templateUrl: './nannydetails.page.html',
  styleUrls: ['./nannydetails.page.scss'],
})
export class NannydetailsPage implements OnInit {

  hidepopup = false;
  nanny_id: any = '';
  isLoading = false;
  data: any;
  isDisabled: boolean = false;
  nannyData: any = [];
  nannyGalleryData: any = [];
  nannyReviewData: any = [];
  profile_image: any = '';
  username: any = '';
  email: any = '';
  phone: any = '';
  userAge: any = '';
  address: any = '';
  state: any = '';
  experience: any = '';
  rate: any = '';
  rate_type: any = '';
  city: any = '';
  zip: any = '';
  avg_rate: any = '';

  showabout = false;
  abactive = "";
  showgallery = true;
  glactive = "activetab";
  showreview = false;
  rvactive = "";
  startDate: any;
  startTime: any;
  endDate: any;
  endTime: any;
  paypalData: any;
  order_final_total: any;
  userID: any;
  userName: any;
  availabilities: any = [];
  userdata: any = [];

  constructor(
    public storage: Storage,
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute,
    public authService: AuthServiceProvider,
    public toastController: ToastController,
    public loadingController: LoadingController,
  ) { 
    this.activatedRoute.queryParams.subscribe((res) => {
      this.nanny_id = res.nanny_id;
      console.log("nanny id: ", this.nanny_id);
    })
  }

  ngOnInit() {}

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;        
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);          
        } else {
          this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 3000);
        }
        this.hideLoader();
      },
      error => {
          this.hideLoader();
      });
    });    
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.nannyDetails();
    this.loginToken();
  }

  nannyDetails(){
    this.storage.get('userDetails').then((val) => {
      var body1 = {
        user_id: val.user_id,
        nany_id: this.nanny_id
      };
      this.authService.postData("get-nany-list", body1).then(result => {
        this.data = result;
        this.nannyData = this.data.result.data;
        this.profile_image = this.nannyData[0].profile_image;
        this.username = this.nannyData[0].fname + ' ' + this.nannyData[0].lname;
        this.email = this.nannyData[0].email;
        this.phone = this.nannyData[0].phone;
        this.userAge = this.splitAge(this.nannyData[0].age);
        this.address = this.nannyData[0].address;
        this.experience = this.nannyData[0].experience;
        this.avg_rate = this.nannyData[0].avg_rate;
        this.rate = this.nannyData[0].rate;
        this.rate_type = this.nannyData[0].rate_type;
        this.state = this.nannyData[0].state;
        this.city = this.nannyData[0].city;
        this.zip = this.nannyData[0].zip;
        this.availabilities = this.nannyData[0].availabilities;
        console.log("Nanny Details: ", this.data);
        var body2 = {
          user_id: this.nanny_id
        };
        this.authService.postData("get-photo-gallery-images", body2).then(result => {
          this.data = result;
          this.nannyGalleryData = this.data.result.data;
          console.log("nannyGalleryData: ", this.nannyGalleryData);
          var body3 = {
            user_id: val.user_id,
            nany_id: this.nanny_id
          };
          this.authService.postData("get-review", body3).then(result => {
            this.data = result;
            this.nannyReviewData = this.data.result.data;
            console.log("nannyReviewData: ", this.nannyReviewData);
            this.hideLoader();
          },
            error => {
              this.hideLoader();
            });
        },
          error => {
            this.hideLoader();
          });
      },
        error => {
          this.hideLoader();
        });
    });
  }

  about() {
    this.showabout = true;
    this.showgallery = false;
    this.showreview = false;
    this.abactive = "activetab";
    this.glactive = "";
    this.rvactive = "";
  }
  gallery() {
    this.showabout = false;
    this.showgallery = true;
    this.showreview = false;
    this.abactive = "";
    this.glactive = "activetab";
    this.rvactive = "";
  }
  review() {
    this.showabout = false;
    this.showgallery = false;
    this.showreview = true;
    this.abactive = "";
    this.glactive = "";
    this.rvactive = "activetab";
  }

  splitAge(age) {
    var ageArr = age.split(' ');
    return ageArr[0];
  }

  onCheck() {
    var object = {
      nanny_id: this.nanny_id,
      rate_type: this.rate_type,
      rate: this.rate,
      availabilities: JSON.stringify(this.availabilities) 
    }
    this.navCtrl.navigateForward(["/calender-view"], { queryParams: object });
  }

  writeReview(){
    var object = {
      nanny_id: this.nanny_id
    }
    this.navCtrl.navigateForward(["/wrte-review"], { queryParams: object });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
