import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoadingController, ToastController } from '@ionic/angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';

let apiUrl = 'https://devfitser.com/NannyonDemand/dev/api/';

@Injectable()
export class AuthServiceProvider {

  loading: any;
  isDisabled: boolean = false;

  constructor(
    private http: HttpClient,
    private transfer: FileTransfer,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
  ) {

  }

  postData(apiName, data) {
    return new Promise((resolve, reject) => {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
        })
      };
      this.http.post(apiUrl + apiName, data, httpOptions)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  getData(apiName) {
    return new Promise((resolve, reject) => {
      this.http.get(apiUrl + apiName)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }


  fileUpload(targetPath, apiFunc, options) {
    let favoritesURL = apiUrl + apiFunc;
    const fileTransfer: FileTransferObject = this.transfer.create();
    return fileTransfer.upload(targetPath, favoritesURL, options)
      .then(res => res.response)
      .catch();
  }

  showLoader(text) {
    this.loading = this.loadingCtrl.create({
      message: text
    });
    //this.loading.present();
  }

  hideLoader() {
    if (this.loading != null) {
      //this.loading.dismiss().catch(() => { });
    }
  }

}